#pragma once
class TramRunAndDoor
{
private:
	const int MaxTimerRun = 60;
	int timerRun=0;
	bool openDoor=false;
	void runThread();
	bool lighting;

public:
	TramRunAndDoor();
	~TramRunAndDoor();
	void run();
	void door();
	void state();
};

