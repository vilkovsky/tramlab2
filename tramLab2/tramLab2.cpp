﻿#include "pch.h"
#include <iostream>
#include <locale.h>
#include "TramRunAndDoor.h"
#include <Windows.h>

using namespace std;

DWORD WINAPI CheckEscape(LPVOID lpParam) {
	while (GetAsyncKeyState(VK_ESCAPE) == 0) { 
		Sleep(10);
	}
	exit(0);

}

void setOptions() {
	setlocale(LC_ALL, "Russian");
	system("color F0");
}

void task() {
	cout << "Начальное задание:" << endl;
	cout << "Имеется две кнопки : управления: газ и открытие / закрытие дверей.Если двери трамвая закрыты и водитель нажимает газ," << endl;
	cout << "то трамвай начинает движение в пределах одной минуты.Если водитель во время движения нажимает «газ»," << endl;
	cout << "то трамвай дополнительно движется одну минуту.Если двери открыты, то трамвай с места тронутся не может." << endl;
	cout << "Внутри трамвая есть освещение, которое работает во время остановки и отключается во время движения(но не во время простоя с закрытыми дверями)." << endl;
	cout << "Во время движения двери открыться не могут.Если время прошло трамвай останавливается.Начальная конфигурация : трамвай стоит, двери закрыты." << endl;
}

void menu() {
	char key;
	TramRunAndDoor tramRunAndDoor;

	tramRunAndDoor.state();
	do {
		cout << "----------------------------------" << endl;
		cout << "\t1 - газ" << endl;
		cout << "\t2 - открыть/закрыть двери" << endl;
		cout << "\t3 - показать текущее состояние" << endl;
		cout << "\tESC - выход" << endl;
		cout << "\tВаш выбор: ";
		cin >> key;

		switch (key)
		{
		case '1':
			tramRunAndDoor.run();
			break;
		case '2':
			tramRunAndDoor.door();
			break;
		case '3':
			tramRunAndDoor.state();
			break;
		default:
			cout << "Дана не управляющая команда!" << endl;
		}

	} while (true);
}

int main()
{
	setOptions();

	CreateThread(NULL, 0, CheckEscape, NULL, 0, NULL);

	task();
	menu();
}


