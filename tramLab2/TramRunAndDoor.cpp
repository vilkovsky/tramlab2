#include "pch.h"
#include "TramRunAndDoor.h"
#include <iostream>
#include "windows.h"
#include <conio.h>
#include <thread>

using namespace std;

TramRunAndDoor::TramRunAndDoor()
{
	timerRun = 0;
	openDoor = false;
	lighting=false;
}

TramRunAndDoor::~TramRunAndDoor()
{
}

void startRun(int* timerRun);

void TramRunAndDoor::runThread() {
	while (timerRun > 0) {
		cout << "�������� ����������� " << timerRun << " ������" << endl;
		timerRun--;
		Sleep(1000);
	}
}

void TramRunAndDoor::run() {
	if (openDoor) {
		cout << "�������� ������ ������, ������� �����!" << endl;
	}
	else {
		cout << "�������� ����������" << endl;
		if (timerRun == 0) {
			timerRun = MaxTimerRun;
			std::thread thread(startRun, &timerRun);
			thread.detach();

			cout << "�������� ������ � ����� ���������� " << MaxTimerRun << " ������"<< endl;
		}
		else {
			cout << "����� �������� ��������� �� " << MaxTimerRun << " ������" << endl;
			timerRun = MaxTimerRun;
		}
	}
}

void startRun(int* timerRun) {
	int time = *timerRun;

	while (*timerRun > 0) {
		int time = *timerRun;
		time--;
		*timerRun = time;
		Sleep(1000);
	}
	cout << endl << "�������� ���������" << endl;

}

void TramRunAndDoor::door() {
	if (timerRun != 0) {
		cout << "���������� ������� ��������������, ������ ��������" << endl;
	}
	else {
		if (openDoor) {
			cout << "����� �����������!" << endl;
			openDoor = false;
			cout << "����������� ���������" << endl;
			lighting = false;
		}
		else {
			cout << "����� �����������!" << endl;
			openDoor = true;
			cout << "���������� ���������" << endl;
			lighting = true;
		}
	}
}

void TramRunAndDoor::state() {
	if (timerRun == 0)
		cout << "������� �����" << endl;
	else {
		cout << "������� ����" << endl;
		cout << "���������� ����� ��������: " << timerRun << " ������" << endl;
	}

	if (openDoor)
		cout << "����� �������" << endl;
	else
		cout << "����� �������" << endl;

	if (lighting)
		cout << "��������� ��������" << endl;
	else
		cout << "��������� ���������" << endl;
}

